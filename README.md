# transit-gateways/tgw
The transit gateways tgw Terraform code.

# Prerequisite
- Create an S3 bucket to act as your terraform remote state bucket and update `my-tgw-terraform-state` in `backend.tf` with your bucket name.
- Update the values in `vars.tf`.

# Usage
```
terraform init
terraform plan
terraform apply
```

# License and Authors
Authors: [Colm McGuigan](https://gitlab.com/colmmg)
