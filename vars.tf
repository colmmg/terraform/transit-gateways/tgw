variable "master-account-id" {
  description = "The AWS account id of your master account where you have configured your AWS Organization."
}

variable "organization-arn" {
  description = "The ARN of your AWS Organization."
}
