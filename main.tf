provider "aws" {
  region = "us-east-1"
}

resource "aws_ec2_transit_gateway" "example" {
  auto_accept_shared_attachments = "enable"
  description                    = "example"
  tags {
    Name = "example"
}

resource "aws_ram_resource_share" "example" {
  allow_external_principals = false
  name                      = "example"
  tags = {
    Name = "example"
  }
}

resource "aws_ram_principal_association" "example" {
  principal          = "arn:aws:organizations::${var.master-account-id}:organization/${organization-arn}"
  resource_share_arn = "${aws_ram_resource_share.example.arn}"
}

resource "aws_ram_resource_association" "example" {
  resource_arn       = "${aws_ec2_transit_gateway.example.arn}"
  resource_share_arn = "${aws_ram_resource_share.example.arn}"
}
