terraform {
  backend "s3" {
    bucket = "my-tgw-terraform-state"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}
